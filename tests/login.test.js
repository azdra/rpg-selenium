const {By, Key, until} = require('selenium-webdriver');
const {emailGenerated, textGenerated} = require("../index");

module.exports = async function (driver) {
  // Navigate to the login page
  await driver.get('http://localhost:8089/login');

  // Find the email and password input fields
  const emailInput = await driver.findElement(By.id('login_form_email'));
  const passwordInput = await driver.findElement(By.id('login_from_password'));

  // Enter the email and password
  await emailInput.sendKeys(emailGenerated);
  await passwordInput.sendKeys(textGenerated, Key.RETURN);

  await driver.wait(until.elementLocated(By.id('confirmationMessage')), 1000);

  // Assert that the registration was successful
  const confirmationMessage = await driver.findElement(By.id('confirmationMessage')).getText();
  return confirmationMessage === 'Login successful';
}
