const {By, Key, until} = require('selenium-webdriver');
const {emailGenerated, textGenerated} = require("../index");

module.exports = async function (driver) {
  // Navigate to the registration page
  await driver.get('http://localhost:8089/register');

  // Find the firstname, lastname, email, password and pseudonym input fields
  const firstnameInput = await driver.findElement(By.id('register_form_firstname'));
  const lastnameInput = await driver.findElement(By.id('register_form_lastname'));
  const emailInput = await driver.findElement(By.id('login_form_email'));
  const passwordInput = await driver.findElement(By.id('register_form_password'));
  const pseudonymInput = await driver.findElement(By.id('register_form_pseudonym'));

  // Enter firstname, lastname, email, password and pseudonym 
  await firstnameInput.sendKeys('ali');
  await lastnameInput.sendKeys('gator');
  await emailInput.sendKeys(emailGenerated);
  await pseudonymInput.sendKeys('aligator');
  await passwordInput.sendKeys(textGenerated, Key.RETURN);

  await driver.wait(until.elementLocated(By.id('confirmationMessage')), 1000);

  // Assert that the registration was successful
  const confirmationMessage = await driver.findElement(By.id('confirmationMessage')).getText();
  return confirmationMessage === 'Registration successful';
}
