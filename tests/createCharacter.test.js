const {By, Key, until, Select} = require('selenium-webdriver');
const {emailGenerated, textGenerated} = require("../index");

module.exports = async function (driver) {
  // Navigate to the create character page
  await driver.get('http://localhost:8089/create-character');


  // Find the name, strength and speed input fields
  const nameInput = await driver.findElement(By.id('create_character_form_name'));
  const strengthInput = await driver.findElement(By.id('create_character_form_strength'));
  const speedInput = await driver.findElement(By.id('create_character_form_speed'));


  // Enter the name, strength and speed
  await nameInput.sendKeys(textGenerated);
  await strengthInput.sendKeys(12);
  await speedInput.sendKeys(12);

  // Wait for the dropdown options to be loaded
  const dropdown = await driver.wait(until.elementLocated(By.id('create_character_form_game')), 5000);

  // Create a Select object from the dropdown
  const select = await new Select(dropdown);

  const options = await select.getOptions();

  // Get a random index
  const randomIndex = Math.floor(Math.random() * options.length);

  // Select a random option from the dropdown
  await select.selectByIndex(randomIndex);

  // Submit the form
  await driver.findElement(By.css('form')).submit();

  // Wait for the success message
  await driver.wait(until.elementLocated(By.id('confirmationMessage')), 5000);

  // Get the success message element
  const successMessage = await driver.findElement(By.id('confirmationMessage'));

  // Assert the success message content
  const expectedMessage = 'Character successfully created';
  const actualMessage = await successMessage.getText();
  return actualMessage === expectedMessage;
}
