const {By, Key, until} = require('selenium-webdriver');
const {textGenerated} = require("../index");

module.exports = async function (driver) {
  // Navigate to the create game page
  await driver.get('http://localhost:8089/create-game');

  // Find the name and description input fields
  const nameInput = await driver.findElement(By.id('create_game_form_name'));
  const descriptionInput = await driver.findElement(By.id('create_game_form_description'));

  // Enter the name and description
  await nameInput.sendKeys(textGenerated);
  await descriptionInput.sendKeys(textGenerated, Key.RETURN);

  await driver.wait(until.elementLocated(By.id('confirmationMessage')), 1000);

  // Assert that the registration was successful
  const confirmationMessage = await driver.findElement(By.id('confirmationMessage')).getText();
  return confirmationMessage === 'Game successfully created';
}
