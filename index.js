function generateRandomText(length) {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let text = '';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    text += characters.charAt(randomIndex);
  }

  return text;
}

function generateRandomEmail() {
  const emailPrefix = generateRandomText(8);
  const emailDomain = 'example.com';
  return `${emailPrefix}@${emailDomain}`;
}

function textGenerated() {
  return generateRandomText(10);
}

module.exports = {
  emailGenerated: generateRandomEmail(),
  textGenerated: textGenerated()
};


(async function testsHandle() {
  const {Builder} = require('selenium-webdriver');
  const fs = require("fs");
  const path = require("path")
  const firefox = require('selenium-webdriver/firefox');

  const options = new firefox.Options();


  for (file of fs.readdirSync('./tests')) {
    const filePath = path.resolve('./tests', file);
    if (fs.statSync(filePath).isFile() && file.endsWith('.test.js')) {
      const driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build();

      try {
        const res = await require(filePath)(driver);
        if (res) {
          console.log(`Test ${file} passed`);
        } else {
          console.log(`Test ${file} failed`);
        }
      } finally {
        await driver.quit();
      }
    }
  }
})();
